const express = require('express')
const app = express()
var mysql = require("mysql");
const PORT = 5403;

// Create a connection to the database
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "12345678",
  database: "express",
});

// open the MySQL connection
connection.connect((error) => {
  if (error) {
    console.log("A error has been occurred ");
    throw error;
  }
});
//query execution
function query(sql) {
  connection.query(sql, function (error, result) {
    if (error) throw error;
    console.log("Query Executed");
  });
}
//creating a database
function createDB(dbname) {
  var sql = `create database ${dbname}`;
  query(sql);
  console.log("Database  created");
}

app.post("/1", (req, res) => {
  res.send(createDB("express"));
});

//creating a table

function createTable(tablename) {
  var sql =
    "create table " +
    tablename +
    "(Brand_name VARCHAR(30), mileage int(10),location varchar(50));";
  query(sql);
  console.log("table  created");
}

app.post("/2", (req, res) => {
  res.send(createTable("batch"));
});

//creating a record

function createRecord(tablename) {
  var sql =
    "insert into " +
    tablename +
    " values ('bharat benz',12000,'warangal'),('splender',1000,'khammam'),('innova',6000,'vishakapatnam'),('activa',2000,'mumbai')";
  query(sql);
  console.log("record  created");
}

app.post("/3", (req, res) => {
  res.send(createRecord("batch"));
});

//reading database

function readDB(dbname) {
  var sql = "use " + dbname;
  query(sql);
  console.log("reading database");
}
app.post("/4", (req, res) => {
  res.send(readDB("databaseone"));
});

//reading record

function readRecord(tablename) {
  sql = "select * from " + tablename;
  query(sql);
  console.log("reading record");
}
app.post("/5", (req, res) => {
  res.send(readRecord("batch"));
});

//reading table

function readTable(tablename) {
  sql = "desc " + tablename;
  query(sql);
  console.log("reading table");
}
app.post("/6", (req, res) => {
  res.send(readTable("batch"));
});

//update table
function updateTable(tablename) {
  sql = "ALTER TABLE " + tablename + " ADD COLUMN country VARCHAR(20)";
  query(sql);
  console.log("update table");
}
app.put("/7", (req, res) => {
  res.send(updateTable("batch"));
});

//update record
function updateRecord(tablename) {
  sql = "update " + tablename + " set mileage = 1500 where country ='india'";

  query(sql);
  console.log("update record");
}
app.put("/8", (req, res) => {
  res.send(updateRecord("batch"));
});

//delete table
function deleteTable(tablename) {
  sql = "drop table " + tablename;

  query(sql);
  console.log("delete table");
}
app.delete("/9", (req, res) => {
  res.send(deleteTable("batch"));
});

//delete record
function deleteRecord(tablename) {
  sql = "delete from " + tablename;
  query(sql);
  console.log("delete table");
}
app.delete("/10", (req, res) => {
  res.send(deleteRecord("batch"));
});

//delete database
function deleteDatabase(dbname) {
  sql = "drop database" + dbname;
  query(sql);
  console.log("delete database");
}
app.delete("/11", (req, res) => {
  res.send(deleteDatabase("databaseone"));
});

//If Everything goes correct, Then start Express Server
app.listen(5403, () => {
  console.log(
    "Database connection is Ready and " + "Server is Listening on Port ",
    PORT
  );
});